 var display_quotes = document.getElementById('display-quotes');
    var pictures = document.getElementById('pictures').innerHTML;
    var images = ['url("images/img-1.jpg")', 'url("images/img-2.jpg")','url("images/img-3.jpg")', 'url("images/img-4.jpg")','url("images/img-5.jpg")','url("images/img-6.jpg")','url("images/img-8.jpg")', 'url("images/img-9.jpg")', 'url("images/img-10.jpg")' , 'url("images/img-12.jpg")', 'url("images/img-13.jpg")', 'url("images/img-14.jpg")', 'url("images/img-15.jpg")', 'url(""images/img-16.jpg)']               
//var images = ['blue', 'green', 'yellow', 'black'];

// function changeIMage(){
// var image = 0;
// if (image < images.length - 1){
//       image++;
//     }else{
//       image = 0;
//     }
//    console.log(image);
//     document.body.style.backgroundImage = images[image];
  
// }
function changeImage(){
//  for(i=0; i<images.length; i++ ){
    
//     console.log(images[i]);
//         document.getElementById('pictures').style.backgroundImage=images[i];
//         return;    
//   }
var imageIndex = Math.floor(Math.random()*images.length);
document.getElementById('pictures').style.backgroundImage=images[imageIndex];

}

    var quotes = ['Today is life--the only life you are sure of. Make the most of today. Get interested in something. Shake yourself awake. Develop a hobby.', 
    'When one door closes, another opens; but we often look so long and so regretfully upon the closed door that we do not see the one that has opened for us.', 
    'The purpose of life is not to be happy. It is to be useful, to be honorable, to be compassionate, to have it make some difference that you have lived and lived well.', 
    "At the end of their lives, people assess how they've done not in terms of their income but in terms of their spirit, and I beg you to do the same.",
    'Life is not about how fast you run or how high you climb, but how well you bounce.',
    'The tragedy of life is not that it ends so soon, but that we wait so long to begin it.',
    'Life is inherently risky. There is only one big risk you should avoid at all costs, and that is the risk of doing nothing.',
    "The quality of a person's life is in direct proportion to their commitment to excellence, regardless of their chosen field of endeavor."]
    
                 
                                  
  function changeQuote() {                          
var quoteIndex = Math.floor(Math.random()*quotes.length);

display_quotes.innerHTML = quotes[quoteIndex];
changeImage();

}

function addQuote(){
     var input = document.getElementById('add-quote-area');
    var addQuote = input.value;
    quotes.push(addQuote.trim());
    console.log(addQuote);
    input.value = '';
  }